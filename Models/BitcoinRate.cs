﻿using BitcoinRates.CurrencyEnum;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace BitcoinRates.Models
{
    public class BitcoinRate
    {
        public decimal Rate { get; set; }
        public Currency Currency { get; set; }

        public BitcoinRate(Currency currency, decimal rate)
        {
            Currency = currency;
            Rate = rate;
        }
    }
}
