﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BitcoinRates.Models
{
    public class Rates
    {
        public decimal BTC_EUR{ get; set; }
        public decimal BTC_USD { get; set; }
    }
}
