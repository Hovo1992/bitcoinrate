﻿using BitcoinRates.CurrencyEnum;
using BitcoinRates.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace BitcoinRates
{
    public class RatesCollection
    {     
        Dictionary<Currency, BitcoinRate> _dictionaryBTC;
        public async Task<Dictionary<Currency, BitcoinRate>> RequestRates( Currency[] toCurrencies)
        {
            string requestPairs = $"{toCurrencies[0].ToString()}";
            for (int i = 1; i < toCurrencies.Length; i++)
                requestPairs += $",{toCurrencies[i].ToString()}";

            var response = await new HttpClient().GetAsync($"https://free.currconv.com/api/v7/convert?q={requestPairs}&compact=ultra&apiKey=22f98423af86e99d6f12");
            var serializedData = await response.Content.ReadAsStringAsync();

            var deserializRateseObj= JsonConvert.DeserializeObject<Rates>(serializedData);

            _dictionaryBTC=new  Dictionary<Currency, BitcoinRate>()
            {
               {Currency.BTC_EUR,new BitcoinRate(Currency.BTC_EUR,deserializRateseObj.BTC_EUR)},// lsi euroi symbol@ poiskem tali chem karu jogem vorna €
               {Currency.BTC_USD,new BitcoinRate(Currency.BTC_USD,deserializRateseObj.BTC_USD)},    
            };
            return _dictionaryBTC;
        }
    }
}
