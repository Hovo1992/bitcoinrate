﻿using System.Collections.Generic;
using BitcoinRates.CurrencyEnum;
using System.Threading.Tasks;
using BitcoinRates.Models;
using System.Windows;
using System.Globalization;

namespace BitcoinRates
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        #region private variables
        private RatesCollection ratesCollection;
        private Dictionary<Currency, BitcoinRate> _bitcoinRates;
        private Currency[] _currencies ={ Currency.BTC_EUR, Currency.BTC_USD, };
        #endregion
        public MainWindow()
        {
            InitializeComponent();
            ratesCollection = new RatesCollection();
        }
        #region Methods
        private async void UpdateButton_Click(object sender, RoutedEventArgs e)
        {
            await GetRates();
        }
        private async Task GetRates()
        {
          
            _bitcoinRates = await ratesCollection.RequestRates( _currencies);

            Bitcoin_USD.Content = _bitcoinRates[Currency.BTC_USD].Rate.ToString("C2");
            CultureInfo.CurrentCulture = new CultureInfo("fr-FR");
            Bitcoin_EUR.Content = _bitcoinRates[Currency.BTC_EUR].Rate.ToString("C2");
        }
        #endregion
    }
}
  