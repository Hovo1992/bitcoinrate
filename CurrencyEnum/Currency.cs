﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BitcoinRates.CurrencyEnum
{
    public enum Currency
    {
        BTC,
        BTC_USD,
        BTC_EUR,    
    }
}
